package com.sd.fpcontactbook;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    ListView lvAllContacts;
    ArrayList<ContactHolder> contactHolderArrayList;
    DBHelper dbHelper;
    ContactHolder contactHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper=new DBHelper(getApplicationContext());
        lvAllContacts=(ListView)findViewById(R.id.lvAllContact);



        //Phone numbers from sim card
        if (dbHelper.isEmpty()){
            Toast.makeText(getApplicationContext(),"Contacts", Toast.LENGTH_SHORT).show();

        }else {
            getContactsFromSimCard();
        }

        populatedContactList();

    }



    public void getContactsFromSimCard(){
        Uri uri= ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection=new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor people=getContentResolver().query(uri,projection,null,null,null);

        int indexName=people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber=people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        people.moveToFirst();
        do{
            String name=people.getString(indexName);
            String number=people.getString(indexNumber);

            ContactHolder values=new ContactHolder(name,number);
            dbHelper.insertContact(values);

        }while (people.moveToNext());
    }

    public void populatedContactList(){
        contactHolderArrayList=dbHelper.getContactList();
        lvAllContacts.setAdapter(new ContactAdapter());
    }



    class ContactAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return contactHolderArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View layout=convertView;
            if (layout==null){
                layout=getLayoutInflater().inflate(R.layout.contacts_structure_layout,parent,false);
            }

            contactHolder=contactHolderArrayList.get(position);

            TextView tvName=(TextView) layout.findViewById(R.id.textViewName);
            TextView tvPhoneNumber=(TextView) layout.findViewById(R.id.textViewPhoneNumber);

            tvName.setText(contactHolder.name);
            tvPhoneNumber.setText(contactHolder.phone);

            return layout;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==100 && resultCode==RESULT_OK){
            populatedContactList();
            Toast.makeText(getApplicationContext(),"Save Contact",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_contact) {
            startActivityForResult(new Intent(MainActivity.this, AddContactActivity.class), 100);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
