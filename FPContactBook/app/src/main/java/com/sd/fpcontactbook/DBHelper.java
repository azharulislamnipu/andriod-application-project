package com.sd.fpcontactbook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Sajib Chandra Das on 6/27/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    final static String DATABASE_NAME="contact_book";
    final static int DATABASE_VERSION=1;

    final static String TABLE_CONTACTS="contacts";
    final static String CONTACT_ID="id";
    final static String CONTACT_NAME="name";
    final static String CONTACT_PHONE="phone";

    final static String QUERY_CREATE_CONTACT="CREATE TABLE IF NOT EXISTS "+TABLE_CONTACTS+" ( "+
            CONTACT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            CONTACT_NAME + " TEXT, "+
            CONTACT_PHONE + " TEXT )";

    final static String QUERY_DROP_CONTACT="CREATE TABLE IF EXISTS "+TABLE_CONTACTS;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(QUERY_CREATE_CONTACT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(QUERY_DROP_CONTACT);
    }

    public long insertContact(ContactHolder values){

        SQLiteDatabase db=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(CONTACT_NAME,values.name);
        contentValues.put(CONTACT_PHONE,values.phone);

        long ret=db.insert(TABLE_CONTACTS,null,contentValues);
        return ret;

    }



    public ArrayList<ContactHolder> getContactList(){

        ArrayList<ContactHolder> contactHolderArrayList=new ArrayList<>();
        SQLiteDatabase db=getReadableDatabase();


        // Ordering by Name
        Cursor cursor = db.query(TABLE_CONTACTS, null, null, null, null, null, CONTACT_NAME+" ASC");
        cursor.moveToFirst();

        for (int i=0;i<cursor.getCount();i++){

            int id=cursor.getInt(cursor.getColumnIndex(CONTACT_ID));
            String name=cursor.getString(cursor.getColumnIndex(CONTACT_NAME));
            String phone=cursor.getString(cursor.getColumnIndex(CONTACT_PHONE));

            ContactHolder contactHolder=new ContactHolder(id,name,phone);
            contactHolderArrayList.add(contactHolder);
            cursor.moveToNext();

        }

        return contactHolderArrayList;

    }



    public boolean isEmpty(){

        String query="SELECT "+CONTACT_ID+" FROM "+TABLE_CONTACTS +";";
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=db.rawQuery(query,null);
        int columnCount=cursor.getCount();
        if (columnCount>=1){
            db.close();
            return true;
        }
        else {
            db.close();
            return false;
        }

    }

}
