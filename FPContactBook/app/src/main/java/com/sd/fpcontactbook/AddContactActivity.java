package com.sd.fpcontactbook;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class AddContactActivity extends ActionBarActivity {

    EditText etName,etPhoneNo;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        dbHelper=new DBHelper(getApplicationContext());
        etName=(EditText)findViewById(R.id.etName);
        etPhoneNo=(EditText)findViewById(R.id.etPhoneNumber);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SaveContact(View view) {

        String name=etName.getText().toString().trim();
        String phone=etPhoneNo.getText().toString().trim();

        ContactHolder values=new ContactHolder(name,phone);
        if (dbHelper.insertContact(values)!=-1){
            setResult(RESULT_OK);
            finish();
        }
    }
}
