package com.sd.fpcontactbook;

/**
 * Created by Sajib Chandra Das on 6/27/2015.
 */
public class ContactHolder {

    public int id;
    public String name,phone;

    public ContactHolder(int id,String name, String phone){

        this.id=id;
        this.name=name;
        this.phone=phone;
    }

    public ContactHolder(String name, String phone){

        this.name=name;
        this.phone=phone;
    }
}
