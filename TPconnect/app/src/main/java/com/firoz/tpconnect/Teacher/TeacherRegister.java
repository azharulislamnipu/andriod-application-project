package com.firoz.tpconnect.Teacher;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.firoz.tpconnect.Constants;
import com.firoz.tpconnect.MainActivity;
import com.firoz.tpconnect.R;
import com.firoz.tpconnect.RequestHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TeacherRegister extends AppCompatActivity implements View.OnClickListener{
    private EditText edfirstName,edlastName,eduserName,edemail,edpassword,edmobileNumber,einitial, eposition;
    private ProgressDialog progressDialog;
    private Button regiBtton;

    TextView user_login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_register);
        regiBtton = (Button) findViewById(R.id.regiButton);

        edfirstName= (EditText) findViewById(R.id.first_name);
        edlastName= (EditText) findViewById(R.id.last_name);
        edmobileNumber= (EditText) findViewById(R.id.mobile_no);
        eduserName= (EditText) findViewById(R.id.username);
        edemail= (EditText) findViewById(R.id.email);
        edpassword= (EditText) findViewById(R.id.password);
        einitial= (EditText) findViewById(R.id.tcrinitial);
        eposition= (EditText) findViewById(R.id.postion);


        regiBtton = (Button) findViewById(R.id.regiButton);

        user_login= (TextView) findViewById(R.id.user_login);
        user_login.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        regiBtton.setOnClickListener(this);
    }

    public void registerUser(){


        final String firstName= edfirstName.getText().toString().trim();
        final String lastName= edlastName.getText().toString().trim();
        final String mobileNumber= edmobileNumber.getText().toString().trim();
        final String userName= eduserName.getText().toString().trim();
        final String teacher_initial= einitial.getText().toString().trim();
        final String teacher_position= eposition.getText().toString().trim();
        final String email= edemail.getText().toString().trim();
        final String password= edpassword.getText().toString().trim();

        progressDialog.setMessage("Registering Teacher.....!!");
        progressDialog.show();




        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.URL_REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();


                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Toast.makeText(getApplicationContext(),jsonObject.getString("message"),Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.hide();
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("firstName",firstName);
                params.put("lastName",lastName);
                params.put("initial",teacher_initial);
                params.put("position",teacher_position);
                params.put("mobileNumber",mobileNumber);
                params.put("username",userName);
                params.put("email",email);
                params.put("password",password);
                return params;
            }
        };

        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.user_login:
                Intent i = new Intent(this, TeacherLogin.class);
                startActivity(i);
                break;

            case R.id.regiButton:
                registerUser();
                break;
            default:
                break;
        }
    }


    @Override
public void onBackPressed(){

        super.onBackPressed();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
}
}
