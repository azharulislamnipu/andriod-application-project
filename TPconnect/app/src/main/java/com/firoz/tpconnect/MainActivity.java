package com.firoz.tpconnect;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;

import com.firoz.tpconnect.Result.ResultActivity;
import com.firoz.tpconnect.Teacher.TeacherLogin;
import com.firoz.tpconnect.Tpf.TpfActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList<Item> gridArray = new ArrayList<Item>();
    CustomGridViewAdapter customGridAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Bitmap teacherIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.teacher);
        Bitmap parentIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.parents);
        Bitmap studentIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.students);
        Bitmap resultIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.result);
        Bitmap cgpaIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.tpf);
        Bitmap crIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.cr);

        gridArray.add(new Item(teacherIcon,"Teacher"));
        gridArray.add(new Item(parentIcon,"Parents Room"));
        gridArray.add(new Item(studentIcon,"Students"));
        gridArray.add(new Item(resultIcon,"Academic Result"));
        gridArray.add(new Item(cgpaIcon,"TP Forum"));
        gridArray.add(new Item(crIcon,"Class Representative"));


        gridView = (GridView) findViewById(R.id.gridView1);
        customGridAdapter = new CustomGridViewAdapter(this, R.layout.row_grid, gridArray);
        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    Intent myIntent = new Intent(view.getContext(), TeacherLogin.class);
                    startActivityForResult(myIntent, 0);
                }

                if (position == 1) {
                    Intent myIntent = new Intent(view.getContext(), TeacherLogin.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 3) {

                    Intent myIntent = new Intent(view.getContext(), ResultActivity.class);
                    startActivityForResult(myIntent, 0);
                }
                if (position == 4) {

                    Intent myIntent = new Intent(view.getContext(), TpfActivity.class);
                    startActivityForResult(myIntent, 0);
                }

            }
        });


    }

    @Override
        public void onBackPressed(){


        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Are you sure want to Exit?");
        builder.setCancelable(true);
        builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();

            }
        });
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
