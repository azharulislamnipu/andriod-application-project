package com.firoz.tpconnect;

/**
 * Created by Azharul Islam Nipu on 1/29/2017.
 */

public class Constants {
    public static final String ROOT_URL="http://192.168.0.117/tpconnect/v1/";
    public static final String URL_REGISTER= ROOT_URL+"registerTeacher.php";
    public static final String URL_LOGIN= ROOT_URL+"teacherlogin.php";

}
