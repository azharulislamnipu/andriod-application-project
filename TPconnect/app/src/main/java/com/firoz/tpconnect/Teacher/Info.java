package com.firoz.tpconnect.Teacher;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.firoz.tpconnect.R;

public class Info extends AppCompatActivity {

    private TextView FirstName,Tcr_position,Email,TeacheInitial,MobileNo,Fullname;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Fullname = (TextView)findViewById(R.id.name);
        Email = (TextView)findViewById(R.id.email);
        TeacheInitial = (TextView)findViewById(R.id.tcher_initial);
        MobileNo = (TextView)findViewById(R.id.mobile);
        Tcr_position = (TextView)findViewById(R.id.tchrpositionshow);

        Fullname.setText(SharedPrefManager.getInstance(this).getFirstName()+" "+SharedPrefManager.getInstance(this).getLastName());
        Email.setText(SharedPrefManager.getInstance(this).getUserEmail());
        TeacheInitial.setText(SharedPrefManager.getInstance(this).getIntial());
        MobileNo.setText(SharedPrefManager.getInstance(this).getMobilenumber());
        Tcr_position.setText(SharedPrefManager.getInstance(this).getPosition());

    }
}
