package com.firoz.tpconnect.Teacher;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.firoz.tpconnect.Constants;
import com.firoz.tpconnect.MainActivity;
import com.firoz.tpconnect.R;
import com.firoz.tpconnect.RequestHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TeacherLogin extends AppCompatActivity implements View.OnClickListener {
    private EditText edusername, edpassword;
    private Button LoginButton;
    private ProgressDialog progressDialog;
    private TextView new_account;
    private boolean isUserClickedBackButton = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher);
        LoginButton = (Button) findViewById(R.id.login_button);
        edusername = (EditText) findViewById(R.id.edtextusername);
        edpassword = (EditText) findViewById(R.id.edtextpassword);


        new_account = (TextView) findViewById(R.id.new_account);
        new_account.setOnClickListener(this);

        LoginButton.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait......!");

        if(SharedPrefManager.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this,TeacherProfile.class));
            return;
        }

        if (!isNetworkAvailable()) {
            // Create an Alert Dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            // Set the Alert Dialog Message
            builder.setMessage("Internet Connection Required")
                    .setCancelable(false)
                    .setPositiveButton("Retry",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    // Restart the Activity
                                    Intent intent = new Intent(
                                            TeacherLogin.this,
                                            MainActivity.class);
                                    finish();
                                    startActivity(intent);
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            new_account.setVisibility(View.VISIBLE);
            // Capture Button click
            new_account.setOnClickListener(new OnClickListener() {

                public void onClick(View arg0) {
                    // Recheck Network Connection
                    if (!isNetworkAvailable()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                TeacherLogin.this);
                        builder.setMessage("Internet Connection Required")
                                .setCancelable(false)
                                .setPositiveButton("Retry",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int id) {
                                                new_account.setVisibility(View.GONE);
                                                // Restart the activity
                                                Intent intent = new Intent(
                                                        TeacherLogin.this,
                                                        TeacherLogin.class);

                                                startActivity(intent);
                                                finish();

                                            }

                                        });
                        AlertDialog alert = builder.create();
                        alert.show();

                    } else {
                        // Open Android Browser

                        sign_up();
                    }

                }
            });
        }


    }

    private void sign_up() {

        Intent i = new Intent(this, TeacherRegister.class);
        startActivity(i);
    }


    private void teacherLogin() {
        final String username = edusername.getText().toString().trim();
        final String password = edpassword.getText().toString().trim();
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                Constants.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (!jsonObject.getBoolean("error")) {

                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(

                                        jsonObject.getInt("id"),
                                        jsonObject.getString("username"),
                                        jsonObject.getString("email"),
                                        jsonObject.getString("firstName"),
                                        jsonObject.getString("lastName"),
                                        jsonObject.getString("initial"),
                                        jsonObject.getString("position"),
                                        jsonObject.getString("mobileNumber")


                                );

                                Toast.makeText(getApplicationContext(), "Teacherer Login Success", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(getApplicationContext(),TeacherProfile.class));
                                finish();

                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(
                                getApplicationContext(),
                                error.getMessage(),
                                Toast.LENGTH_LONG
                        ).show();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        RequestHandler.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.new_account:

                break;

            case R.id.login_button:
                teacherLogin();
                break;
            default:
                break;
        }
    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                //finish();
//                onBackPressed();
//                break;
//        }
//        return true;
//    }

    @Override
    public void onBackPressed(){

        super.onBackPressed();
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }

//    @Override
//public void onBackPressed(){
//
//    if(!isUserClickedBackButton){
//        Toast.makeText(this,"Press Back again to exit", Toast.LENGTH_LONG).show();
//        isUserClickedBackButton=true;
//    }else {
//
//        super.onBackPressed();
//        finish();
//    }
//    new CountDownTimer(3000,1000){
//
//        @Override
//        public void onTick(long l) {
//
//        }
//
//        @Override
//        public void onFinish() {
//            isUserClickedBackButton=false;
//        }
//    }.start();
//}


//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        switch(keyCode){
//            case KeyEvent.KEYCODE_BACK:
//                // do something here
//                Intent intent = new Intent(TeacherLogin.this,
//                        MainActivity.class);
//                startActivity(intent);
//                return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }


    private boolean isNetworkAvailable() {
        // Using ConnectivityManager to check for Network Connection
        ConnectivityManager connectivityManager = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

}
