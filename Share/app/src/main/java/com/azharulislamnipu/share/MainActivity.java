package com.azharulislamnipu.share;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity {
    EditText text,subject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

         text = (EditText) findViewById(R.id.text);
        subject = (EditText) findViewById(R.id.sub);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //noinspection SimplifiableIfStatement

        switch(item.getItemId()) {
            case R.id.share:

                try {
                    String stringtext = text.getText().toString();
                    String stringsub = subject.getText().toString();
                    Intent shareintent = new Intent();
                    shareintent.setType("Text/plain");
                    shareintent.putExtra(Intent.EXTRA_SUBJECT,stringsub );
                    shareintent.putExtra(Intent.EXTRA_TEXT, stringtext);
                    startActivity(Intent.createChooser(shareintent,"Share Via"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.action_settings:
                return true;


        }

        return super.onOptionsItemSelected(item);
    }
}
