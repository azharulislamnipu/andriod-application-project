package com.ypyproductions.materialdialogs;


public enum DialogAction {
    POSITIVE,
    NEUTRAL,
    NEGATIVE
}
