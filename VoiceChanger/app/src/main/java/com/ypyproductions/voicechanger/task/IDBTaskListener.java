package com.ypyproductions.voicechanger.task;


public abstract interface IDBTaskListener {
	
	public abstract void onPreExcute();
	public abstract void onPostExcute();
	public abstract void onDoInBackground();
}
