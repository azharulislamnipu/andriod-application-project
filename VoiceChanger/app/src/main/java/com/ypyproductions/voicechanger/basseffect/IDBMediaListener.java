package com.ypyproductions.voicechanger.basseffect;


public abstract interface IDBMediaListener {
	public abstract void onMediaCompletion();
	public abstract void onMediaError();
}
